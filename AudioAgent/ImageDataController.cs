﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using MetadataExtractor;

namespace AudioAgent
{
    public class ImageDataController : ApiController
    {
        // GET api/<controller>
        public IHttpActionResult GetImageMetaData(string Url)
        {
            try
            {
                var directories = ImageMetadataReader.ReadMetadata(Url);
                return ExtractImageMetaData(directories, "Using ImageMetadataReader");
            }
            catch
            {
                throw;
            }
        }

        // Extract the image metadata
        IHttpActionResult ExtractImageMetaData(IEnumerable<MetadataExtractor.Directory> directories, string method)
        {
            var jsonResponse = new List<object>();

            // Extraction gives us potentially many directories
            foreach (var directory in directories)
            {
                // Each directory stores values in tags
                foreach (var tag in directory.Tags)
                {
                    System.Diagnostics.Debug.WriteLine(tag);
                    jsonResponse.Add(new { Name = tag.Name, Description = tag.Description });
                }

                // Each directory may also contain error messages
                foreach (var error in directory.Errors)
                    Console.Error.WriteLine("ERROR: " + error);             // Logging errors to a file can be used in a production environment
            }

            return Json(jsonResponse);
        }

        // Logging errors to a file can be used in a production environment
        void PrintError(Exception exception) => Console.Error.WriteLine($"EXCEPTION: {exception}");
    }
}