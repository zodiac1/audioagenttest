﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;

namespace AudioAgent
{
    public partial class Default : System.Web.UI.Page
    {
        protected void btnGetImageMetaData_Click(object sender, EventArgs e)
        {
            try
            {
                // This example ignores the discrepancy between server side code and a file located on the client PC
                if (File.Exists(txtImageUrl.Text))
                {
                    ImageDataController controller = new ImageDataController();
                    IHttpActionResult result = controller.GetImageMetaData(txtImageUrl.Text);

                    string jsonResult = new JavaScriptSerializer().Serialize((result as JsonResult<List<object>>).Content);
                    lblResult.Text = jsonResult;
                }
                else
                {
                    throw new FileNotFoundException(txtImageUrl.Text);
                }
            }
            catch (Exception ex)
            {
                // Logging errors to a file can be used in a production environment
                Console.Error.WriteLine("ERROR: " + ex.ToString());
                lblResult.Text = ex.ToString();
            }
        }
    }
}