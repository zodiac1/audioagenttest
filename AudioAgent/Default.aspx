﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AudioAgent.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Audio Agent</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" ID="lblImageUrl">Image Url:</asp:Label>
            <asp:TextBox runat="server" ID="txtImageUrl" Width="900px"></asp:TextBox>
            <asp:Button runat="server" ID="btnGetImageMetaData" Text="Get Image MetaData" OnClick="btnGetImageMetaData_Click" />
        </div>
        <div style="margin-top:50px">
            <asp:Label runat="server" ID="lblResult" Width="1024px"></asp:Label>
        </div>
    </form>
</body>
</html>
